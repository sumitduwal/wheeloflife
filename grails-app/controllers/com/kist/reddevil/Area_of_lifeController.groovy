package com.kist.reddevil



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class Area_of_lifeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Area_of_life.list(params), model:[area_of_lifeInstanceCount: Area_of_life.count()]
    }

    def show(Area_of_life area_of_lifeInstance) {
        respond area_of_lifeInstance
    }

    def create() {
        respond new Area_of_life(params)
    }

    @Transactional
    def save(Area_of_life area_of_lifeInstance) {
        if (area_of_lifeInstance == null) {
            notFound()
            return
        }

        if (area_of_lifeInstance.hasErrors()) {
            respond area_of_lifeInstance.errors, view:'create'
            return
        }

        area_of_lifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'area_of_life.label', default: 'Area_of_life'), area_of_lifeInstance.id])
                redirect area_of_lifeInstance
            }
            '*' { respond area_of_lifeInstance, [status: CREATED] }
        }
    }

    def edit(Area_of_life area_of_lifeInstance) {
        respond area_of_lifeInstance
    }

    @Transactional
    def update(Area_of_life area_of_lifeInstance) {
        if (area_of_lifeInstance == null) {
            notFound()
            return
        }

        if (area_of_lifeInstance.hasErrors()) {
            respond area_of_lifeInstance.errors, view:'edit'
            return
        }

        area_of_lifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Area_of_life.label', default: 'Area_of_life'), area_of_lifeInstance.id])
                redirect area_of_lifeInstance
            }
            '*'{ respond area_of_lifeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Area_of_life area_of_lifeInstance) {

        if (area_of_lifeInstance == null) {
            notFound()
            return
        }

        area_of_lifeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Area_of_life.label', default: 'Area_of_life'), area_of_lifeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'area_of_life.label', default: 'Area_of_life'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
