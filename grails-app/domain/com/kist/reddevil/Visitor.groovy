package com.kist.reddevil

class Visitor {
	String firstName
	String lastName
	String email

    static constraints = {
		firstName blank:false, nullable:false, size:3..25
		lastName blank:false, nullable:false, size:3..25
		email email:true,blank:false
		
    }
}
